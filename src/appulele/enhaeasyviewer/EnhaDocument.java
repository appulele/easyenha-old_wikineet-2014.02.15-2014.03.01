package appulele.enhaeasyviewer;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EnhaDocument {
	Context context;
	private String tabName = "";
	private int bgColor = 0xffffffff;
	private LinearLayout tabLayout;
	private Button btnText;
	private Button btnClose;
	private ImageView imgTabDivi;
	private Object tag;
	MainActivity mAct;
	private Boolean tabCheck = true;
	private TextView tv;
	private ArrayList<EnhaDocument> alChildrenTab;
	private EnhaDocument parent = null;
	private WebView webView;
	String TABINORDER = "tab in order";
	private EnhaDocument enhaDocBrother;
	/*
	 * tab이 가져야 할 하위 목록 parent childrenArrayList WebView, LinearLayout(Close
	 * Btn, Name TextView(버튼이 나을까?))
	 */

	// tabCheck으로 상단 탭인지, 또는 좌측 추가 탭인지 구분한다.
	public EnhaDocument(Context context, boolean tabTopOrLeft) {
		setAlChildrenTab(new ArrayList<EnhaDocument>());
		imgTabDivi = new ImageView(context);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		LayoutParams paramsWrap = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		imgTabDivi.setLayoutParams(paramsWrap);
		imgTabDivi.setBackgroundResource(R.drawable.ic_launcher);
		this.setTabCheck(tabTopOrLeft);
		this.context = context;
		mAct = (MainActivity) context;
		setLayout(new LinearLayout(context));
		btnText = new Button(context);
		tv = new TextView(context);
		setBtnClose(new Button(context));
		LayoutParams lParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.MATCH_PARENT);

		if (getTabCheck() == false) {
			lParams = new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT);
		}
		getTab().setLayoutParams(lParams);
		// btnText 의 태그로 tab자신을 보낸다.
		btnText.setTag((EnhaDocument) this);
		btnText.setTextSize(10);
		btnText.setLayoutParams(lParams);

		// btn.setSingleLine();
		btnText.setMaxLines(2);
		getBtnClose().setVisibility(View.GONE);

		if (getTabCheck() == false) {
			getBtnClose().setVisibility(View.GONE);
			lParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			getBtnClose().setLayoutParams(lParams);
			getBtnClose().setBackgroundResource(R.drawable.ic_launcher);
			
			
		} else if (getTabCheck() == true) {
			getBtnClose().setVisibility(View.GONE);
			getBtnClose().setLayoutParams(lParams);
			getBtnClose().setBackgroundResource(
					android.R.drawable.button_onoff_indicator_off);
			imgTabDivi.setVisibility(View.GONE);
		}
		btnClose.setTag(null);
		getTab().addView(imgTabDivi);
		getTab().addView(getBtnClose()); 
		getTab().addView(btnText);
		

	 }

	public int getBgColor() {
		return bgColor;
	}

	public void setBgColor(int bgColor) {
		this.bgColor = bgColor;
		getTab().setBackgroundColor(bgColor);
		btnText.setBackgroundColor(bgColor);
	}

	public String getTabName() {
		return tabName;

	}

	public void setTabName(String tabName) {
		this.tabName = tabName;
		btnText.setText(tabName);
	}

	public Object getTag() {
		return tag;
	}

	public void setTag(Object tag) {
		this.tag = tag;
		btnText.setTag(tag);

		tv.setTag(tag);
		tabLayout.setTag(tag);

	}

	public void setListener() {
		btnText.setOnClickListener(mAct);
		getBtnClose().setOnClickListener(mAct);

	}

	public CharSequence getTv() {
		return tv.getText();
	}

	public void setTv(CharSequence cs) {
		this.tv.setText(cs);

	}

	public LinearLayout getTab() {
		return tabLayout;
	}

	public void setLayout(LinearLayout layout) {
		this.tabLayout = layout;

	}

	public EnhaDocument getParent() {
		return parent;
	}

	public boolean hasParent() {
		boolean value = false;
		if (parent != null)
			value = true;
		return value;
	}

	public void setParent(EnhaDocument parent) {
		this.parent = parent;

	}

	public ArrayList<EnhaDocument> getAlChildrenTab() {
		return alChildrenTab;
	}

	public void addAlChildrenTab(int index, EnhaDocument childrTab) {
		this.getAlChildrenTab().add(index, childrTab);
	}

	public WebView getWebView() {
		return webView;
	}

	public void setWebView(WebView webView) {
		this.webView = webView;
	}

	// mActivity의 tabLayout에 자신과 자식의 탭을 추가한다.(Not use)
	public void setTabView() {
		// mAct.refreshTabLayout((Tab)this);
		Log.d(TABINORDER, "setTabViewtParent:" + (EnhaDocument) this.getTag());
		for (EnhaDocument tabChild : getAlChildrenTab()) {
			tabChild.setTabView();
			Log.d(TABINORDER,
					"setTabViewtChild:" + (EnhaDocument) this.getTag());
		}

	}

	// 현재 탭에 다른 탭을 자식으로 추가한다.
	public void addChildTab(EnhaDocument tab) {
		// 최상위 탭을 추가한다.
		getAlChildrenTab().add(tab);
		for (EnhaDocument tabChild : getAlChildrenTab()) {
			Log.d(TABINORDER, "in addChildTab," + this.getTabName()
					+ "`s child is " + tabChild.getTabName());
		}
	}

	
	public void addTabToLayout(int padding) { 
		mAct.llTabMid.addView(this.getTab());
		for (EnhaDocument tabChild : getAlChildrenTab()) {
			Log.d(TABINORDER, tabChild.getTabName() + "`s child count is "
					+ tabChild.getAlChildrenTab().size());
			
			tabChild.imgTabDivi.setPadding(padding,padding,padding,padding);
			tabChild.addTabToLayout(padding);

		}
	}

	public void addTabSideToLayout(int padding){
		padding =padding+ 100;
		mAct.llTabNavi.addView(this.getTab());
		for (EnhaDocument tabChild : getAlChildrenTab()) {
			Log.d(TABINORDER, tabChild.getTabName() + "`s child count is "
					+ tabChild.getAlChildrenTab().size());
			tabChild.addTabSideToLayout(padding);
			
			
		

		}
	}

	public void setBtnCloseVisible(boolean bool) {
		if (bool)
			getBtnClose().setVisibility(View.VISIBLE);
		else
			getBtnClose().setVisibility(View.GONE);
	}

	public Button getBtnClose() {
		return btnClose;
	}

	public void setBtnClose(Button btnClose) {
		this.btnClose = btnClose;
	}

	public void setAlChildrenTab(ArrayList<EnhaDocument> alChildrenTab) {
		this.alChildrenTab = alChildrenTab;
	}

	public boolean hasChildren() {
		boolean hasChild = false;
		if (alChildrenTab.size() != 0)
			hasChild = true;
		return hasChild;
	}

	public EnhaDocument getEnhaDocBrother() {
		return enhaDocBrother;
	}

	public void setEnhaDocBrother(EnhaDocument enhaDocBrother) {
		this.enhaDocBrother = enhaDocBrother;
		 
	}

	public Boolean getTabCheck() {
		return tabCheck;
	}

	public void setTabCheck(Boolean tabCheck) {
		this.tabCheck = tabCheck;
	}
 
}
