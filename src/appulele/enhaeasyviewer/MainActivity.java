package appulele.enhaeasyviewer;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern; 
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Toast;

@SuppressLint("JavascriptInterface")
public class MainActivity extends Activity implements OnClickListener, OnKeyListener, OnCheckedChangeListener {
	private final String TAG = this.getClass().getSimpleName();
	// 부모를 가지고 있으면 1, 없으면 최상위로 0
	public static int TYPE_HIGHIST = 0;
	public static int TYPE_HAS_PARENTS = 1;
	Context mContext;
	LinearLayout llMain;
	LinearLayout llMainFind;
	LinearLayout llMainAddWebView;
	LinearLayout llTabMid;
	LinearLayout llTabNavi;
	int curFindNum = 0;

	EditText etMain;
	ArrayList<WebView> alWebView;
	Button btnFind;
	Button btnMove;
	Button btnSelected;
	Button btnRandom;
	Button btnChanged;
	Button btnDarkAndLight;
	Button btnMinimalize;

	String tag = "MainActivity";
	String TABINORDER = "tab in order";
	
	//모드에 따라 모바일/일반 변환 
	//String urlMode = "http://m.enha.kr/wiki/";
	String urlMode = "http://mirror.enha.kr/wiki/";
	// ArrayList<Tab> alTab;
	// 부모 탭이 되는 탭들의 List, 탭 검색,이동, 링크로 탭 추가, 탭 삭제 동작시에 변한다.
	ArrayList<EnhaDocument> tabsHighistList;
	ArrayList<EnhaDocument> naviHighistList;
	EnhaDocument tabCurrent;

	//모바일 페이지로 볼지, 일반 페이지로 볼지 설정 
	private final boolean MORBILE_PAGE = false;
	private final boolean NORMAL_PAGE =true;
	boolean curPageMode =MORBILE_PAGE;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.activity_main);
		/*
		 * llMain:메인 레이아웃 llMainFind 찾기관련 레이아웃 etMain 에딧창 btnFind 검색버튼 btnMove
		 * 이동버튼 alWebView 웹뷰 어레이 리스트
		 */
		llMain = (LinearLayout) findViewById(R.id.linearLayoutMain);
		llMainFind = (LinearLayout) findViewById(R.id.linearLayoutMainFind);
		llMainAddWebView = (LinearLayout) findViewById(R.id.linearLayoutMainAddWebView);
		llTabMid = (LinearLayout) findViewById(R.id.linearLayoutViewTab);
		llTabNavi = (LinearLayout) findViewById(R.id.llTNaviab);
		etMain = (EditText) findViewById(R.id.eTMainFind);
		etMain.setText("FBI");
		etMain.setOnKeyListener(this);
		btnFind = (Button) findViewById(R.id.btnMainFind);
		btnFind.setOnClickListener(this);
		btnDarkAndLight = (Button) findViewById(R.id.btnMainLightAndDark);
		btnDarkAndLight.setOnClickListener(this);
		btnMinimalize = (Button) findViewById(R.id.btnMainMinimalize);
		btnMinimalize.setOnClickListener(this);
		btnRandom = (Button) findViewById(R.id.btnMainRandom);
		btnRandom.setOnClickListener(this);
		btnChanged = (Button) findViewById(R.id.btnMainChanged);
		btnChanged.setOnClickListener(this);
		btnMove = (Button) findViewById(R.id.btnMainMove);
		btnMove.setOnClickListener(this);
		alWebView = new ArrayList<WebView>();
		tabsHighistList = new ArrayList<EnhaDocument>();
		naviHighistList = new ArrayList<EnhaDocument>();
		btnSelected = new Button(mContext);
	}

	// 버튼 클릭시
	@Override
	public void onClick(View btn) {
		btnSelected = (Button) btn;
		switch (btnSelected.getId()) {
		// 찾기 버튼을 눌렀을 때 검색어가 비어있지 않으면 검색
		case R.id.btnMainFind:
			if (!etMain.getText().toString().equals("") && !etMain.getText().toString().equals(null)) { 
				GetJSoupTask gJSoup = new GetJSoupTask(mContext, urlMode+"/search/?q="
						+ etMain.getText().toString(), etMain.getText().toString(), 0);
				gJSoup.execute();
				etMain.setText("");
			}
			break;
		// 이동 버튼을 눌렀을 때 검색어가 비어있지 않으며 이동
		case R.id.btnMainMove:
			if (!etMain.getText().toString().equals("") && !etMain.getText().toString().equals(null)) {
				String keyword = etMain.getText().toString();
				String encodedKeyword = "";
				try {
					encodedKeyword = URLEncoder.encode(etMain.getText().toString(), "UTF-8");
					encodedKeyword.replace('+', ' ');
					Log.d("encodeKeyword", "keyword:" + encodedKeyword);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				Log.d(tag, "Move URL: " + "http://m.enha.kr/wiki/" + encodedKeyword + "  keyword:" + keyword); 
				GetJSoupTask gJSoup = new GetJSoupTask(mContext, urlMode + encodedKeyword, etMain
				.getText().toString(), 0);
				
				gJSoup.execute();
				etMain.setText("");
			}
			break;
		// 랜덤
		case R.id.btnMainRandom: 
			GetJSoupTask gJSoup = new GetJSoupTask(mContext, "http://m.enha.kr/random", etMain.getText().toString(), 0);
			gJSoup.execute();
			break;

		// 어두운/밝은 화면
		case R.id.btnMainLightAndDark:

			break;

		// 최소화
		case R.id.btnMainMinimalize:

			break;

		// 바뀐글
		case R.id.btnMainChanged:

			break;
		default:
			Log.d(tag, "currentClick`s tag is :" + btnSelected.getTag());
			if (tabCurrent != null)
				tabCurrent.setBtnCloseVisible(false);

			if (btnSelected.getTag() != null) {
				EnhaDocument tabParent = (EnhaDocument) btnSelected.getTag();
				llMainAddWebView.removeAllViews();
				llMainAddWebView.addView(tabParent.getWebView());
				// 탭을 클릭시 현재 탭 기억
				tabCurrent = tabParent;
				if (tabCurrent.getTabCheck() == false)
					tabCurrent = tabCurrent.getEnhaDocBrother();
				tabCurrent.setBtnCloseVisible(true);
				Log.d(tag, "currentTab:" + tabCurrent);
			} else {
				// 클릭 된 "버튼"의 태그가 null-> closeBtn 이다.
				// 부모가 null로 없는 최상위 탭을 삭제할 때에는 alTabHighist 와 alNaviHighist에서
				// 삭제를 해준다.
				// 부모가 존재하는 자식 탭을 삭제 할 때에는 존재하는 자식을 현재 자신의 포지션 넘버에 이동시킨다.
				LinearLayout LLParent = (LinearLayout) btnSelected.getParent();
				EnhaDocument enhaDocContent = (EnhaDocument) (LLParent.getChildAt(2)).getTag();
				EnhaDocument enhaDocNavi = enhaDocContent.getEnhaDocBrother();
				// 삭제를 누른 탭이 부모가 있으면 부모 탭에서 자식 탭을 삭제 후 , 삭제 전에 자식 탭이 있으면 부모 탭으로
				// 올려줌
				if (enhaDocContent.hasParent()) {
					Log.d(tag, "부모가 있다. ");

					EnhaDocument enhaDocParent = enhaDocContent.getParent();
					ArrayList<EnhaDocument> parentChildList = enhaDocParent.getAlChildrenTab();
					ArrayList<EnhaDocument> childList = enhaDocContent.getAlChildrenTab();

					EnhaDocument enhaNaviParent = enhaDocNavi.getParent();
					ArrayList<EnhaDocument> parentNaviChildList = enhaNaviParent.getAlChildrenTab();
					ArrayList<EnhaDocument> childNaviList = enhaDocNavi.getAlChildrenTab();

					for (int i = 0; i < parentChildList.size(); i++) {
						// 부모의 자식 목록에서 선택한 탭을 찾으면, 선택한 탭의 자식 목록을 부모의 자식목록에 추가한다.
						Log.d(tag, "부모의 자식이 있다.");
						if (parentChildList.get(i) == enhaDocContent) {
							for (int j = 0; j < childList.size(); j++) {
								childList.get(i).setParent(enhaDocParent);
								parentChildList.add(i + 1, childList.get(j));

								childNaviList.get(i).setParent(enhaNaviParent);
								parentNaviChildList.add(i + 1, childNaviList.get(j));
							}
						}
						parentChildList.remove(i);
						enhaDocParent.setAlChildrenTab(parentChildList);

						parentNaviChildList.remove(i);
						enhaNaviParent.setAlChildrenTab(parentNaviChildList);

						refreshTabLayout();
					}

				}
				// 부모 탭이 없으면 최상위에서 삭제하고 소유한 자식들이 있을 경우 자식들을 최상위로 끌어올린다.
				else {
					// if (enhaDocContent.hasChildren()) {
					Log.d(tag, "부모가 없고, 자식이 있다면 자식을 최상위로 끌어올린다. ");
					for (int i = tabsHighistList.size() - 1; i >= 0; i--) {
						if (tabsHighistList.get(i) == enhaDocContent) {
							for (int j = 0; j < enhaDocContent.getAlChildrenTab().size(); j++) {
								EnhaDocument child = enhaDocContent.getAlChildrenTab().get(j);
								tabsHighistList.add(j + 1, child);
								
								EnhaDocument childNavi = enhaDocNavi.getAlChildrenTab().get(j);
								naviHighistList.add(j + 1, childNavi);
							}
							tabsHighistList.remove(i);
							 naviHighistList.remove(i);
							llMainAddWebView.removeAllViews();
							if (tabsHighistList.size() != 0) {
								tabCurrent = tabsHighistList.get(i);
								if (tabCurrent.getTabCheck() == false)
									tabCurrent = tabCurrent.getEnhaDocBrother();
								tabCurrent.setBtnCloseVisible(true);
								llMainAddWebView.addView(tabCurrent.getWebView());
							}
							refreshTabLayout();
							break;
						}
					}
				}
				Log.d(tag, "enhaDocContent: " + enhaDocContent);
			}
			break;
		}
	}

	// 현재 탭뷰의 넘버를 alTab의 태그넘버와 비교해서 같으면 웹뷰를 보여준다.
	public void TabViewRefresh() {
		// Log.d("curFindNum:", "curFindNum is:" + curFindNum);
		// for (int i = 0; i < alTab.size(); i++) {
		//
		// if (curFindNum == (Integer) alTab.get(i).getTag()) {
		// llMainAddWebView.removeAllViews();
		// llMainAddWebView.addView(alWebView.get(i));
		// }
		// }

	}

	public static String deletingNotNeeds(String result) {
		Matcher mat;
		// script 처리
		final Pattern script = Pattern.compile("<(no)?script[^>]*>.*?</(no)?script>", Pattern.DOTALL);
		mat = script.matcher(result);
		result = mat.replaceAll("");

		// head 처리
		Pattern style = Pattern.compile("<head[^>]*>.*</head>", Pattern.DOTALL);
		mat = style.matcher(result);
		result = mat.replaceAll("<head></head>");

		return result;
	}

	// 받아온 값 탭으로 바로 보내기

	public void createWebView(WebView webViewNew, String curSelectedName, int createType) {
		// webViewCreate
		webViewNew.addJavascriptInterface(new Object() {

			// HTML에서 링크 클릭시 새 탭 추가
			@JavascriptInterface
			public void createLinkTab(String text) {
				Toast.makeText(mContext, "create" + text, Toast.LENGTH_SHORT).show();

			}

			// 외부 이미지, iframe(유투브)
			@JavascriptInterface
			public void external(String url) {
				// 해당 url이미지 팝업으로 띄워주기
				Toast.makeText(mContext, "img url:" + url, Toast.LENGTH_SHORT).show();
				// url의 문자열에서 youtube를 발견하면 동영상으로(아직 미구현), 아니면 이미지를 보여준다.
				ExternalNewDialog.newDialog(mContext, url);
			}

			// 외부 링크
			@JavascriptInterface
			public void externalLink(String url) {
				// 해당 url웹뷰 팝업으로 띄워주기
				Toast.makeText(mContext, "외부 링크", Toast.LENGTH_SHORT).show();
				ExternalLinkNewDialog.newDialog(mContext, url);
			}

			// 주석
			@JavascriptInterface
			public void foot(String text) {
				Log.d(tag, "주석: " + text);
				// 해당 주석 팝업 띄워주기
				FootNewDialog.newDialog(mContext, text);
			}

			// 위키 페이지 이동
			@JavascriptInterface
			public void wiki(String str) {
				// 해당 위키 페이지로 이동
				Toast.makeText(mContext, str, Toast.LENGTH_SHORT).show();
				Log.d(TAG, str);

				String encodedKeyword = "";
				try {
					encodedKeyword = URLEncoder.encode(str, "utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				// fix2

				GetJSoupTask getJSoup = new GetJSoupTask(mContext, "http://mirror.enha.kr/wiki/" + encodedKeyword, str, 1);
				// GetJSoup getJSoup = new GetJSoup(mContext,
				// "http://m.enha.kr" +url, keyword,
				// 1);

				getJSoup.execute();
			}

		}, "AppInterface");

		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		ScrollView sv = new ScrollView(mContext);
		// 웹뷰로 js alert보기위한 설정
		// fix3 webView 에 태그 달기

		webViewNew.getSettings().setJavaScriptEnabled(true);
		webViewNew.setWebChromeClient(new WebChromeClient());
		// 웹뷰로 새창 열기
		webViewNew.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				return false;
			}
		});

		Log.d("cursel", "cursel:" + curSelectedName);
		CreateTab(curSelectedName, webViewNew, createType);
	}

	// 탭을 새로 만든다. CreateType가 0이면 부모없는 최상위 탭, 1이면 자식 탭이다.
	public void CreateTab(String curSelectedName, WebView webViewNew, int createType) {

		EnhaDocument enhaDoc = new EnhaDocument(mContext, true);
		enhaDoc.setTabName(curSelectedName);// 웹뷰 저장->tab생성->탭 클릭시 뷰 보여주기
		enhaDoc.setWebView(webViewNew);

		EnhaDocument enhaDocSide = new EnhaDocument(mContext, false);
		enhaDocSide.setTabName(curSelectedName);
		enhaDocSide.setWebView(webViewNew);

		// if(tabCurrent!=null)tabCurrent.setBtnCloseVisible(false);
		if (createType == TYPE_HIGHIST) {
			Log.d(TABINORDER, "createType: null, no parents");
			enhaDoc.setParent(null);
			enhaDoc.setListener();
			enhaDoc.setTag(enhaDoc);
			// tab.setBtnCloseVisible(true);

			enhaDocSide.setParent(null);
			enhaDocSide.setListener();
			enhaDocSide.setTag(enhaDocSide);

			enhaDoc.setEnhaDocBrother(enhaDocSide);
			enhaDocSide.setEnhaDocBrother(enhaDoc);

			tabsHighistList.add(enhaDoc);
			naviHighistList.add(enhaDocSide);

		} else if (createType == TYPE_HAS_PARENTS) {
			// fix4 임시로 null, curSelTab을 null대신 넣는다.
			Log.d(TABINORDER, "createType: " + tabCurrent.getTabName() + " is parents");

			enhaDoc.setParent(tabCurrent);
			enhaDoc.setListener();
			enhaDoc.setTag(enhaDoc);

			enhaDocSide.setParent(tabCurrent.getEnhaDocBrother());
			enhaDocSide.setListener();
			enhaDocSide.setTag(enhaDocSide);

			enhaDocSide.setEnhaDocBrother(enhaDoc);
			enhaDoc.setEnhaDocBrother(enhaDocSide);

			tabCurrent.addChildTab(enhaDoc);
			tabCurrent.getEnhaDocBrother().addChildTab(enhaDocSide);

			Log.d(tag, "enhaDocSide: " + enhaDocSide);
			Log.d(tag, "bro: " + tabCurrent.getEnhaDocBrother());
			// !!!!!!!!!!Doc brother 조절해봐야될듯

		}
		refreshTabLayout();

	} 
	// 엔터시에 동작
	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			if (keyCode == event.KEYCODE_ENTER) {
				return true;
			}

		}

		return false;
	}

	public void refreshTabLayout() {
		llTabMid.removeAllViews();
		llTabNavi.removeAllViews();

		for (EnhaDocument tabChild : tabsHighistList) {
			// 각 최상위 탭마다, 자기 자신과 자신의 자식들이 레이아웃에 추가되는 함수
			tabChild.addTabToLayout(0);
		}
		for (EnhaDocument tabChildSide : naviHighistList) {
			// 각 최상위 탭마다, 자기 자신과 자신의 자식들이 레이아웃에 추가되는 함수
			tabChildSide.addTabSideToLayout(0);

		}

		// 검색, 링크순대로 바로바로 추가
		// llTabMid.addView(tab.getLayout());
		// llTabMid.removeAllViews();
		//
		// //자기를 추가, 자식이 있으면 추가로 자식도 추가(재귀메서드로)
		// for (Tab tabChild : alTabsHighist) {
		// llTabMid.addView(tabChild.getLayout());
		// for (Tab tabGrandChild : tabChild.getAlChildrenTab()) {
		// tabGrandChild.
		// }
		// }

	}

	@Override
	public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
		// TODO Auto-generated method stub

	}
}
