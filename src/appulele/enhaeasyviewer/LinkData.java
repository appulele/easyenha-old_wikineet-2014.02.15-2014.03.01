package appulele.enhaeasyviewer;

import org.jsoup.nodes.Element;

public class LinkData {
	private String text;
	private String url;
	private String title;
	private String linkClass;
	
	public LinkData(Element link) {
		this.text = link.text();
		this.url = link.attr("href");
		this.title = link.attr("title");
		this.linkClass = link.attr("class");
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLinkClass() {
		return linkClass;
	}

	public void setLinkClass(String linkClass) {
		this.linkClass = linkClass;
	}
	
	
	
}
