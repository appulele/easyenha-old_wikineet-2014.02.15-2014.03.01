package appulele.enhaeasyviewer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities.EscapeMode;
import org.jsoup.select.Elements;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.webkit.WebView;

public class GetJSoupTask extends AsyncTask<Void, Void, Void> {
	/**
	 * a 태그의 class로 구분한다 wiki 를 포함하면 : Move external : 외부이미지 externalLink : 외부링크
	 * 
	 * a 태그의 href로 구분한다 #fn 을 포함하면 : 주석. 해당 a태그의 title을 출력해준다.
	 */
	private final String TAG = this.getClass().getSimpleName();
	private final boolean D = false;
	private final boolean MORBILE_PAGE = false;
	private final boolean NORMAL_PAGE =true;
	
	private Context mContext;
	private String originalHTML = "";
	private String name = "";
	private MainActivity mActivity;
	private ArrayList<LinkData> mLinkDataList;
	private String url = "";
	int createType = -1;
	boolean curPageMode = NORMAL_PAGE;
	// if type ==0 부모 없음,==1부모 있음
	public GetJSoupTask(Context context, String url, String name, int type) {
		mContext = context;
		mActivity = (MainActivity) context;
		this.name = name;
		this.url = url.replace('+', ' ');
		createType = type;
	}

	@Override
	protected Void doInBackground(Void... params) {
		Document doc = null;
		mLinkDataList = new ArrayList<LinkData>();

		// 해당 url의 HTML 받아옴
		try {
			doc = Jsoup.connect(url).get();  
		} catch (IOException e) {
			e.printStackTrace();
		}
		originalHTML = doc.html();  
		Elements elementLinks = doc.select("a[href]");  
		// HTML내부의 모든 링크를 mLinkDataList에 추가
		for (Element link : elementLinks) {
			LinkData linkData = new LinkData(link);
			mLinkDataList.add(linkData);
		}
	 

		// 링크 담아놓을 ArrayList선언
		ArrayList<LinkData> wikiLinkDataList = new ArrayList<LinkData>();
		ArrayList<LinkData> externalLinkDataList = new ArrayList<LinkData>();
		ArrayList<LinkData> externalLinkLinkDataList = new ArrayList<LinkData>();
		ArrayList<LinkData> footLinkDataList = new ArrayList<LinkData>();

		// HTML 링크 클릭시 함수 동작
		for (int i = 0; i < mLinkDataList.size(); i++) {
			LinkData curLinkData = mLinkDataList.get(i);
			String linkClass = curLinkData.getLinkClass();
			String linkTitle = curLinkData.getTitle();
			String linkUrl = curLinkData.getUrl();

			if (linkClass.equals("wiki")) {
				wikiLinkDataList.add(curLinkData);
			} else if (linkClass.equals("external")) {
				externalLinkDataList.add(curLinkData);
			} else if (linkClass.equals("externalLink")) {
//			} else if (linkClass.contains("externalLink")) {
				externalLinkLinkDataList.add(curLinkData);
			} else if (linkUrl.contains("#fn")) {
				footLinkDataList.add(curLinkData);
			}
		}

		// Log
		if (D) {
			String listNameArray[] = new String[] { "wiki", "external", "externalLink", "foot" };
			ArrayList<ArrayList<LinkData>> listArray = new ArrayList<ArrayList<LinkData>>();
			listArray.add(wikiLinkDataList);
			listArray.add(externalLinkDataList);
			listArray.add(externalLinkLinkDataList);
			listArray.add(footLinkDataList);

			for (int i = 0; i < listArray.size(); i++) {
				ArrayList<LinkData> curLinkDataList = listArray.get(i);
				String type = listNameArray[i];
				Log.d(TAG, "-- " + type + " Start --");
				for (int j = 0; j < curLinkDataList.size(); j++) {
					LinkData curLinkData = curLinkDataList.get(j);
					Log.d(TAG, type + " [" + j + "] : " + curLinkData.getTitle() + ", " + curLinkData.getUrl());
				}
				Log.d(TAG, "-- " + type + " End --");
			}
		}

		/**
		 * href="/wiki/([^\"]+)\""; 인 모든 링크주소 한 번에 바꾸기 ex)
		 * href="/wiki/starcraft" ->
		 * href="javascript:AppInterface.wiki("starcraft")"
		 */
		String wikiRegex = "href=\"/wiki/([^\"]+)\"";
		originalHTML = originalHTML.replaceAll(wikiRegex, "href=\"" + "javascript:AppInterface.wiki(" + "\'$1\'"
				+ ")\"");

		/**
		 * 외부 이미지 링크 함수로 바꾸기
		 * */
		for (int i = 0; i < externalLinkDataList.size(); i++) {
			//외부 주소의 - 문자를 html형으로 변환 
			String url = externalLinkDataList.get(i).getUrl();
			String urlConvert = externalLinkDataList.get(i).getUrl().replace("-","&#45;");//.replace("(", "&#40;").replace(")"," &#41;");
			originalHTML = originalHTML.replaceAll(url, "javascript:AppInterface.external(\'" + urlConvert + "\')");
			
			Log.d("externalLink","externalLinkㄴ URL CONVERT:"+urlConvert);
		}

		/**
		 * 주석 함수로 바꾸기
		 * */
		for (int i = footLinkDataList.size()-1; i >=0; i--) {
			String url = footLinkDataList.get(i).getUrl();
			//String형의 특수문자를 HTML의 특수 문자로 변환한다.
			String text = footLinkDataList.get(i).getTitle().replace("\"", "&quot;").replace("\'", "&quot;");
			Log.d("	footLinkData Title : ", "name: " + footLinkDataList.get(i).getText() + "  title: " + text);
			Log.d("	footLinkData Title : ", "url: " + footLinkDataList.get(i).getUrl());
			originalHTML = originalHTML.replaceAll(url, "javascript:AppInterface.foot(\'" + text + "\')");
		}
		
		/**
		 * 외부 링크 웹뷰로 띄우기
		 * */
		for (int i = 0; i < externalLinkLinkDataList.size(); i++) {
			String url = externalLinkLinkDataList.get(i).getUrl();
			String text = externalLinkLinkDataList.get(i).getTitle(); 
			originalHTML = originalHTML.replaceAll(url, "javascript:AppInterface.externalLink(\'" + url + "\')");
		}
		// HTML내부의 필요없는 부분 지우기 
		//일반화면 and 모바일 화면 
		if(curPageMode==MORBILE_PAGE){
		originalHTML=	originalHTML.replace("<div class=\"toolbar\">",
				"<div class=\"toolbar\" style=\"visibility: hidden; display:inline;>");
		originalHTML=originalHTML.replace("<div class=\"toolbox\">",
				"<div class=\"toolbox\" style=\"visibility: hidden; display:inline;>");
		}else if(curPageMode ==NORMAL_PAGE){
			originalHTML=	originalHTML.replace("<div class=\"boilerplate\">",
					"<div class=\"boilerplate\" style=\"visibility: hidden; display:inline;>");
			originalHTML=	originalHTML.replace("<div class=\"search-box\">",
					"<div class=\"search-box\" style=\"visibility: hidden; display:inline;>");
			originalHTML=originalHTML.replace("<div id=\"_toolbox\">",
					"<div id=\"_toolbox\" style=\"visibility: hidden; display:inline;>");
		}
		 
		return null;
	}

	// Post Execute
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		Log.d("	onPostExecute", "name: " + name);
		WebView wvContent = new WebView(mContext);
		wvContent.loadData(originalHTML, "text/html; charset=utf-8", "utf-8");
		// type 0,1을 구분해서 동작
		mActivity.createWebView(wvContent, name, createType);
	}

}
